import React, { Component } from 'react';
import PropTypes from 'prop-types';

// import CommunityButton from './CommunityButton';
import User from '../models/User';

import { connect } from 'react-redux';
import { selectUser } from '../redux/reducers/userSlice';

import OnlyRole from '../components/OnlyRole';

import { CREATE_DAC_ROLE, CREATE_CAMPAIGN_ROLE } from "../constants/Role";


/**
 * The join Giveth community top-bar
 */
class JoinGivethCommunity extends Component {
  constructor(props) {
    super(props);

    this.createDAC = this.createDAC.bind(this);
    this.createCampaign = this.createCampaign.bind(this);
  }

  createDAC() {
    const { currentUser } = this.props;
    const isDelegate = currentUser && currentUser.isDelegate();

    if (!isDelegate) {
      React.swal({
        title: 'Sorry, this Dapp is in beta...',
        content: React.swal.msg(
          <p>
            It&#8217;s great to see that you want to start a Decentralized Fund. However, this Dapp
            is still in beta and we only allow a select group of people to start Funds
            <br />
            Please <strong>contact us on our Slack</strong>, or keep browsing
          </p>,
        ),
        icon: 'info',
        buttons: [false, 'Got it'],
      });
      return;
    }

    if (this.props.currentUser) {
      this.props.history.push('/dacs/new');
    } else {
      React.swal({
        title: "You're almost there...",
        content: React.swal.msg(
          <p>
            It&#8217;s great to see that you want to start a Decentralized Fund. To get started,
            please sign up (or sign in) first.
          </p>,
        ),
        icon: 'info',
        buttons: ['Cancel', 'Sign up now!'],
      }).then(isConfirmed => {
        if (isConfirmed) this.props.history.push('/signup');
      });
    }
  }

  createCampaign() {
    const { currentUser } = this.props;
    const isCampaignManager = currentUser && currentUser.isCampaignManager();

    if (!isCampaignManager) {
      React.swal({
        title: 'Sorry, this Dapp is in beta...',
        content: React.swal.msg(
          <p>
            It&#8217;s great to see that you want to start a campaign, however, this Dapp is still
            in beta and we only allow a select group of people to start campaigns
            <br />
            Please <strong>contact us on our Slack</strong>, or keep browsing
          </p>,
        ),
        icon: 'info',
        buttons: [false, 'Got it'],
      });
      return;
    }
    if (this.props.currentUser) {
      this.props.history.push('/campaigns/new');
    } else {
      React.swal({
        title: "You're almost there...",
        content: React.swal.msg(
          <p>
            It&#8217;s great to see that you want to start a campaign. To get started, please sign
            up (or sign in) first.
          </p>,
        ),
        icon: 'info',
        buttons: ['Cancel', 'Sign up now!'],
      }).then(isConfirmed => {
        if (isConfirmed) this.props.history.push('/signup');
      });
    }
  }

  render() {
    let cantBanners = 4;
    let randomBackgroundNumber = Math.ceil(Math.random() * cantBanners);
    return (
      <div
        id="join-giveth-community"
        style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/img/banner${randomBackgroundNumber}.jpeg)` }}
      >
        <div className="vertical-align">
          <center>
            <h3><br /></h3>
            <OnlyRole role={CREATE_DAC_ROLE}>
              <button type="button" className="btn btn-info" onClick={() => this.createDAC()}>
                Crear una DAC
              </button>
            </OnlyRole>
            <OnlyRole role={CREATE_CAMPAIGN_ROLE}>
              <button type="button" className="btn btn-info" onClick={() => this.createCampaign()}>
                Iniciar una campa&ntilde;a
              </button>
            </OnlyRole>
          </center>
        </div>
      </div>
    );
  }
}

JoinGivethCommunity.propTypes = {
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
  }).isRequired,
  currentUser: PropTypes.instanceOf(User),
};

JoinGivethCommunity.defaultProps = {
  currentUser: undefined,
};

const mapStateToProps = (state, props) => ({
  currentUser: selectUser(state)
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(JoinGivethCommunity);